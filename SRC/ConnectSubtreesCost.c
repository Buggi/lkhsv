#include "LKH.h"

/*
Hier wird nicht der 1-Tree benutzt sondern die zus�tzliche Kante wird weggelassen, damit immer 2 echte Teilb�ume entstehen k�nnen wenn die Kante (From, To) entfernt wird.
Der MST ohne die zus�tzliche Kante l�sst sich durch das TreeNeighbours Attribut ermitteln.

*/


static void GetNeighbours(Node* N, Node* Nprev, Array* Tree);
static void initArray(Array* a, size_t initialSize);
static void insertArray(Array* a, Node element);
static void freeArray(Array* a);
int ConnectSubtreesCost(Node *From, Node *To) 
{
	int Cost, CheapestCost = INT_MAX;
	Array Tree1, Tree2;
	initArray(&Tree1, 10);
	insertArray(&Tree1, *From);
	//printff("\nTree1: %d", From->Id);
	GetNeighbours(From, To, &Tree1);

	initArray(&Tree2, 10);
	insertArray(&Tree2, *To);
	//printff("Tree2: %d", To->Id);
	GetNeighbours(To, From, &Tree2);

	for (int i = 0; i < Tree1.used; i++)
	{
		for (int j = 0; j < Tree2.used; j++)
		{
			if (Tree1.array[i].Id == From->Id && Tree2.array[j].Id == To->Id ||
				Tree1.array[i].Id == To->Id && Tree2.array[j].Id == From->Id)
				continue;

			if (Tree1.array[i].Id == Tree2.array[j].Id)
				continue;

			//Die zus�tzliche Kante vom 1-tree darf nicht als Verbindung der Teilb�ume genutzt werden
			if (Tree1.array[i].Id == FirstNode->Id && Tree2.array[j].Id == FirstNode->Next->Id ||
				Tree1.array[i].Id == FirstNode->Next->Id && Tree2.array[j].Id == FirstNode->Id)
				continue;

			Cost = D(&Tree1.array[i], &Tree2.array[j]);
			if (Cost < CheapestCost)
			{
				//printff("{%d,%d}\n", Tree1.array[i].Id, Tree2.array[j].Id);
				CheapestCost = Cost;
			}
		}
	}
	freeArray(&Tree1);
	freeArray(&Tree2);
	return CheapestCost;
}

/*
 *
*/
static void GetNeighbours(Node* N, Node* Nprev, Array* Tree)
{
	Neighbour* TN = N->TreeNeighbours;
	Node *To;
	
	for (TN = N->TreeNeighbours; (To = TN->To); TN++)
	{
		if (Nprev == To)
			continue;
		//printff("-%d", To->Id);
		insertArray(Tree, *To);
		GetNeighbours(To, N, Tree);
	}
}


/*https://stackoverflow.com/a/3536261/10263863 */
void initArray(Array* a, size_t initialSize) {
	a->array = (Node*)malloc(initialSize * sizeof(Node));
	a->used = 0;
	a->size = initialSize;
}

void insertArray(Array* a, Node element) {
	// a->used is the number of used entries, because a->array[a->used++] updates a->used only *after* the array has been accessed.
	// Therefore a->used can go up to a->size 
	if (a->used == a->size) {
		a->size *= 2;
		a->array = (Node*)realloc(a->array, a->size * sizeof(Node));
	}
	a->array[a->used++] = element;
}

void freeArray(Array* a) {
	free(a->array);
	a->array = NULL;
	a->used = a->size = 0;
}
